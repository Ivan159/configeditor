package controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import main.FileEditDialog;
import util.NodeProperty;

public class FileEditWindowController {

	private final FileEditDialog dialog;
	private final Map<String, Map<String, String>> changedProperties = new HashMap<>();
	private final Map<String, String> changedResources = new HashMap<>();
	private String changedDialect;
	private final Map<String, String> changedSysProps = new HashMap<>();
	private final Map<String, String> changedLdapSettings = new HashMap<>();
	private final Map<String, String> changedLdapGroups = new HashMap<>();

	@FXML
	private GridPane connectionPoolPane;
	@FXML
	private ComboBox<String> connPropSelectCB;
	@FXML
	private GridPane resourcePane;
	@FXML
	private ComboBox<String> dialectCB;
	@FXML
	private GridPane systemPropertiesGP;
	@FXML
	private Label systemNameLabel;
	@FXML
	private Label resourceNameLabel;
	@FXML
	private Label connNameLabel;
	@FXML
	private Label ldapSettingsLabel;
	@FXML
	private GridPane ldapSettingsGP;
	@FXML
	private Label ldapGroupsLabel;
	@FXML
	private GridPane ldapGroupsGP;

	public FileEditWindowController(FileEditDialog dialog) {
		this.dialog = dialog;
	}

	@FXML
	private void initialize() {
		this.connPropSelectCB.getItems()
				.addAll(this.dialog.dbConnectionPoolPropertyMap.keySet());
		AtomicInteger atomicInteger = new AtomicInteger(1);
		this.connPropSelectCB.valueProperty()
				.addListener((a, o, n) -> {
					this.rebuildConnPollPane(n);
				});
		this.dialectCB.getItems()
				.addAll(this.dialog.availableDialects);
		this.dialectCB.setValue(this.dialog.dialectProperty.getValue());
		this.dialectCB.valueProperty()
				.addListener((a, o, n) -> {
					if (this.dialog.dialectProperty.getValue()
							.equals(n)) {
						this.dialectCB.setStyle("-fx-font-weight: normal");
					} else {
						this.dialectCB.setStyle("-fx-font-weight: bold");
					}
					if (!n.equals(o)) {
						this.changedDialect = n;
					}
				});
		atomicInteger.set(1);

		this.dialog.systemPropertiesMap.forEach((k, v) -> {
			Label nameLabel = new Label(k);
			TextField textField = new TextField(v.getValue());
			textField.textProperty()
					.addListener((a, o, n) -> {
						if (v.getValue()
								.equals(n)) {
							textField.setStyle("-fx-font-weight: normal");
						} else {
							textField.setStyle("-fx-font-weight: bold");
						}
						if (!n.equals(o)) {
							this.changedSysProps.put(k, n);
						}
					});
			this.systemPropertiesGP.addRow(atomicInteger.getAndIncrement(), nameLabel, textField);
		});
		this.dialog.dbResourceNodeMap.forEach((name, node) -> {
			ComboBox<String> dbResourceCb = new ComboBox<>();
			dbResourceCb.setMaxWidth(Double.MAX_VALUE);
			dbResourceCb.valueProperty()
					.addListener((a, o, n) -> {
						if (node.getValue()
								.equals(n)) {
							dbResourceCb.setStyle("-fx-font-weight: normal");
						} else {
							dbResourceCb.setStyle("-fx-font-weight: bold");
						}
						this.connPropSelectCB.setValue(n);
						this.changedResources.put(node.getName(), n);
					});
			dbResourceCb.getItems()
					.addAll(this.dialog.dbConnectionPoolPropertyMap.keySet());
			Label nameLabel = new Label(node.getName());
			dbResourceCb.setValue(node.getValue());
			this.resourcePane.addRow(atomicInteger.getAndIncrement(), nameLabel, dbResourceCb);
		});
		atomicInteger.set(1);

		Map<String, NodeProperty> ldapSettingsMap = this.dialog.ldapSettingsMap;
		this.ldapSettingsGP.setManaged(!ldapSettingsMap.isEmpty());
		this.ldapSettingsGP.setVisible(!ldapSettingsMap.isEmpty());
		this.ldapSettingsLabel.setManaged(!ldapSettingsMap.isEmpty());
		this.ldapSettingsLabel.setVisible(!ldapSettingsMap.isEmpty());
		ldapSettingsMap.forEach((k, v) -> {
			Label nameLabel = new Label(k);
			TextField textField = new TextField(v.getValue());
			textField.textProperty()
					.addListener((a, o, n) -> {
						if (v.getValue()
								.equals(n)) {
							textField.setStyle("-fx-font-weight: normal");
						} else {
							textField.setStyle("-fx-font-weight: bold");
						}
						if (!n.equals(o)) {
							this.changedLdapSettings.put(k, n);
						}
					});
			this.ldapSettingsGP.addRow(atomicInteger.getAndIncrement(), nameLabel, textField);
		});

		Map<String, NodeProperty> ldapGroupsMap = this.dialog.ldapGroupsMap;
		this.ldapGroupsGP.setManaged(!ldapGroupsMap.isEmpty());
		this.ldapGroupsGP.setVisible(!ldapGroupsMap.isEmpty());
		this.ldapGroupsLabel.setManaged(!ldapGroupsMap.isEmpty());
		this.ldapGroupsLabel.setVisible(!ldapGroupsMap.isEmpty());
		ldapGroupsMap.forEach((k, v) -> {
			Label nameLabel = new Label(k);
			TextField textField = new TextField(v.getValue());
			textField.textProperty()
					.addListener((a, o, n) -> {
						if (v.getValue()
								.equals(n)) {
							textField.setStyle("-fx-font-weight: normal");
						} else {
							textField.setStyle("-fx-font-weight: bold");
						}
						if (!n.equals(o)) {
							this.changedLdapGroups.put(k, n);
						}
					});
			this.ldapGroupsGP.addRow(atomicInteger.getAndIncrement(), nameLabel, textField);
		});
	}

	private void rebuildConnPollPane(String newPool) {
		this.connectionPoolPane.getChildren()
				.removeIf(node -> GridPane.getRowIndex(node) != null);
		AtomicInteger atomicInteger = new AtomicInteger(1);
		Label label = new Label("ConnectionURL");
		BooleanProperty isMSSQLProperty = new SimpleBooleanProperty(newPool.contains("mssql"));
		StringProperty hostProperty = new SimpleStringProperty("");
		StringProperty portProperty = new SimpleStringProperty("");
		StringProperty dbNameProperty = new SimpleStringProperty("");
		TextField field = new TextField();
		field.setEditable(false);
		this.connectionPoolPane.addRow(atomicInteger.getAndIncrement(), label, field);
		field.textProperty()
				.bind(Bindings.createStringBinding(() -> {
					if (isMSSQLProperty.get()) {
						return "jdbc:sqlserver://" + hostProperty.get() + ":" + portProperty.get() + ";databaseName=" + dbNameProperty.get();
					} else {
						return "jdbc:postgresql://" + hostProperty.get() + ":" + portProperty.get() + "/" + dbNameProperty.get();
					}
				}, isMSSQLProperty, hostProperty, portProperty, dbNameProperty));
		this.dialog.dbConnectionPoolPropertyMap.get(newPool)
				.forEach(nodeProperty -> {
					if (nodeProperty != null) {
						Label nameLabel = new Label(nodeProperty.getName());

						TextField textField = new TextField(nodeProperty.getValue());
						textField.textProperty()
								.addListener((a, o, n) -> {
									if (!o.equals(n)) {
										if (nodeProperty.getValue()
												.equals(n)) {
											textField.setStyle("-fx-font-weight: normal");
										} else {
											textField.setStyle("-fx-font-weight: bold");
										}
										Map<String, String> changedProperty = this.changedProperties.getOrDefault(newPool, new HashMap<>());
										changedProperty.put(nodeProperty.getName(), n);
										this.changedProperties.put(newPool, changedProperty);
									}
								});
						if (nodeProperty.getName()
								.equals("ServerName")) {
							hostProperty.bind(textField.textProperty());
						} else if (nodeProperty.getName()
								.equals("PortNumber")) {
							portProperty.bind(textField.textProperty());
						} else if (nodeProperty.getName()
								.equals("DatabaseName")) {
							dbNameProperty.bind(textField.textProperty());
						}
						this.connectionPoolPane.addRow(atomicInteger.getAndIncrement(), nameLabel, textField);
					}
				});
	}

	private boolean acceptCpChanges() {
		boolean changed = false;
		for (Entry<String, Map<String, String>> e : this.changedProperties.entrySet()) {
			String k = e.getKey();
			Map<String, String> v = e.getValue();
			List<NodeProperty> poolValues = this.dialog.dbConnectionPoolPropertyMap.get(k);
			for (Entry<String, String> entry : v.entrySet()) {
				String k1 = entry.getKey();
				String v1 = entry.getValue();
				for (NodeProperty property : poolValues) {
					if (property.getName()
							.equals(k1)) {
						String oldValue = property.getValue();
						if (!Objects.equals(oldValue, v1)) {
							changed = true;
							property.setValue(v1);
							System.out.println("changed:\t" + k + "." + property.getName() + "\t\twas: " + oldValue + "\tnow: " + v1);
						}
						break;
					}
				}
			}
		}
		return changed;
	}

	private boolean acceptResChanges() {
		boolean changed = false;
		for (Entry<String, String> entry : this.changedResources.entrySet()) {
			String k = entry.getKey();
			String v = entry.getValue();
			NodeProperty resource = this.dialog.dbResourceNodeMap.get(k);
			String oldValue = resource.getValue();
			if (!Objects.equals(oldValue, v)) {
				changed = true;
				resource.setValue(v);
				System.out.println("changed:\t" + k + "\t\twas: " + oldValue + "\tnow: " + v);
			}
		}
		return changed;
	}

	private boolean acceptSysChanges() {
		boolean changed = false;
		for (Entry<String, String> entry : this.changedSysProps.entrySet()) {
			String k = entry.getKey();
			String v = entry.getValue();
			NodeProperty property = this.dialog.systemPropertiesMap.get(k);
			String oldValue = property.getValue();
			if (!Objects.equals(oldValue, v)) {
				property.setValue(v);
				changed = true;
				System.out.println("changed:\t" + property.getName() + "\t\twas: " + oldValue + "\tnow: " + v);
			}
		}
		if (this.changedDialect != null) {
			String oldValue = this.dialog.dialectProperty.getValue();
			if (Objects.equals(this.changedDialect, oldValue)) {
				return false;
			}
			this.dialog.dialectProperty.setValue(this.changedDialect);
			changed = true;
			System.out.println("changed: dialect\t\twas: " + oldValue + "\tnow: " + this.changedDialect);
		}
		return changed;
	}

	public boolean acceptChanges() {
		return this.acceptSysChanges() | this.acceptResChanges() | this.acceptCpChanges() | this.acceptLdapChanges();
	}

	private boolean acceptLdapChanges() {
		boolean changed = false;
		for (Entry<String, String> entry : this.changedLdapSettings.entrySet()) {
			String k = entry.getKey();
			String v = entry.getValue();
			NodeProperty property = this.dialog.ldapSettingsMap.get(k);
			String oldValue = property.getValue();
			if (!Objects.equals(oldValue, v)) {
				property.setValue(v);
				changed = true;
				System.out.println("changed:\t" + property.getName() + "\t\twas: " + oldValue + "\tnow: " + v);
			}
		}
		for (Entry<String, String> entry : this.changedLdapGroups.entrySet()) {
			String k = entry.getKey();
			String v = entry.getValue();
			NodeProperty property = this.dialog.ldapGroupsMap.get(k);
			String oldValue = property.getValue();
			if (!Objects.equals(oldValue, v)) {
				property.setValue(v);
				changed = true;
				System.out.println("changed:\t" + property.getName() + "\t\twas: " + oldValue + "\tnow: " + v);
			}
		}
		return changed;
	}

}
