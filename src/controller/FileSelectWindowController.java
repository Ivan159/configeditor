package controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.MultipleSelectionModel;
import javafx.scene.control.SelectionMode;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import main.FileEditDialog;
import util.FileUtil;

public class FileSelectWindowController {

	public Path FILE_LIST_PATH;

	{
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			try {
				FileUtil.writeFile(this.FILE_LIST_PATH, this.fileList.getItems().stream().map(Path::toString).collect(Collectors.toList()));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}));
	}

	@FXML private ListView<Path> fileList;
	@FXML private Button editButton;
	@FXML private Button addButton;
	@FXML private Button removeButton;

	private Path selectedPath;
	private Stage stage;

	public FileSelectWindowController() {
		this.FILE_LIST_PATH = Paths.get("pathList.txt");
	}

	@FXML
	private void initialize() {
		MultipleSelectionModel<Path> selectionModel = this.fileList.getSelectionModel();
		selectionModel.setSelectionMode(SelectionMode.SINGLE);
		this.selectedPath = selectionModel.getSelectedItem();
		selectionModel.selectedItemProperty().addListener((observable, oldVal, newVal) ->
				this.selectedPath = newVal
		);
		try {
			Stream<Path> pathStream = FileUtil.getLinesStream(this.FILE_LIST_PATH).map(Paths::get).filter(Files::isRegularFile);
			pathStream.forEach(p -> this.fileList.getItems().add(p));
		} catch (IOException e) {
			System.out.println("no pathList available in" + this.FILE_LIST_PATH);
		}
	}

	@FXML
	private void onEditAction() {
		if (this.selectedPath == null || !Files.isRegularFile(this.selectedPath)) {
			return;
		}
		Optional<String> result;
		result = new FileEditDialog(this.selectedPath).showAndWait();
		result.ifPresent(r -> {
			try {
				FileUtil.writeFile(this.selectedPath, r);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
	}

	@FXML
	private void onAddAction() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("select your File:");
		fileChooser.setInitialDirectory(new File("C:/"));
		File file = fileChooser.showOpenDialog(this.stage);
		if (file != null && file.isFile()) {
			this.fileList.getItems().add(0, file.toPath());
			this.selectedPath = file.toPath();
		}
	}

	@FXML
	private void onRemoveAction() {
		if (this.selectedPath == null || !Files.isRegularFile(this.selectedPath)) {
			return;
		}
		final ObservableList<Path> itemList = this.fileList.getItems();
		itemList.remove(this.selectedPath);
		this.selectedPath = itemList.isEmpty() ? null : itemList.get(0);
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}
}
