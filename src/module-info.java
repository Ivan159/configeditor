module ConfigEditorGlassfish {
	requires javafx.controls;
	requires javafx.fxml;
	requires java.xml;
	exports main to javafx.graphics;
	opens controller to javafx.fxml;
}