package util;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XMLUtil {

	public Document getDocument() {
		return this.document;
	}

	private final Document document;

	public XMLUtil(Path docPath) throws ParserConfigurationException, IOException, SAXException {
		this.document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(docPath.toFile());
	}

	private static List<Node> getElementsByName(String name, Node startElement) {
		List<Node> resultList = new ArrayList<>();
		if (name.contains(".")) {
			String startElementName = name.substring(0, name.indexOf("."));
			String newPath = name.substring(name.indexOf(".") + 1);
			List<Node> startList = getChildNodesByName(startElement, startElementName);
			for (int i = 0; i < startList.size(); i++) {
				resultList.addAll(getElementsByName(newPath, startList.get(0)));
			}
		}
		resultList.addAll(getChildNodesByName(startElement, name));
		return resultList;
	}

	public List<Node> getSystemProperties() {
		return  getElementsByName("servers.server.system-property", this.document.getDocumentElement());
	}

	public List<Node> getDBResourceNodes() {
		List<Node> resourceNodes = getElementsByName("resources.jdbc-resource", this.document.getDocumentElement());
		Attribute dbResourceAttribute1 = new Attribute("jndi-name", "jdbc/qep");
		Attribute dbResourceAttribute2 = new Attribute("jndi-name", "jdbc/qep/target");
		return getNodesWithMatchingAttributes(resourceNodes, dbResourceAttribute1, dbResourceAttribute2);
	}

	public List<Node> getConnectionPools() {
		Attribute attribute1 = new Attribute("res-type", "javax.sql.DataSource");
		Attribute attribute2 = new Attribute("res-type", "javax.sql.ConnectionPoolDataSource");
		List<Node> unfilteredList = getElementsByName("resources.jdbc-connection-pool", this.document.getDocumentElement());
		return getNodesWithMatchingAttributes(unfilteredList, attribute1, attribute2);
	}

	private static List<Node> getNodesWithMatchingAttributes(List<Node> nodeList, Attribute... attributes) {
		List<Node> resultList = new ArrayList<>();
		for (Node parent : nodeList) {
			for (Attribute attribute : attributes) {
				String value = ((Element) parent).getAttribute(attribute.getName());
				if (attribute.getValue().equals(StringUtil.cleanString(value))) {
					resultList.add(parent);
					break;
				}
			}
		}
		return resultList;
	}

	private static List<Node> getChildNodesByName(Node parent, String name) {
		List<Node> elementList = new ArrayList<>();
		if (!parent.hasChildNodes()) {
			return elementList;
		}
		for (int i = 0; i < parent.getChildNodes().getLength(); i++) {
			Node child = parent.getChildNodes().item(i);
			if (child.getNodeName().equals(name)) {
				elementList.add(child);
			}
		}
		return elementList;
	}

	public List<Node> getLdapSettings() {
		List<Node> result = new ArrayList<>();
		List<Node> resourceNodes = getElementsByName("resources.custom-resource", this.document.getDocumentElement());
		Attribute mainLdapAttribute = new Attribute("jndi-name", "jndi/ldap");
		List<Node> mainLdapNodes = getNodesWithMatchingAttributes(resourceNodes, mainLdapAttribute);
		for (Node mainLdapNode : mainLdapNodes) {
			NodeList childNodes = mainLdapNode.getChildNodes();
			int length = childNodes.getLength();
			for (int i = 0; i < length; i++) {
				result.add(childNodes.item(i));
			}
		}
		return result;
	}

	public List<Node> getLdapGroups() {
		List<Node> result = new ArrayList<>();
		List<Node> resourceNodes = getElementsByName("resources.custom-resource", this.document.getDocumentElement());
		Attribute mainLdapAttribute = new Attribute("jndi-name", "jndi/ldapGroup");
		List<Node> mainLdapNodes = getNodesWithMatchingAttributes(resourceNodes, mainLdapAttribute);
		for (Node mainLdapNode : mainLdapNodes) {
			NodeList childNodes = mainLdapNode.getChildNodes();
			int length = childNodes.getLength();
			for (int i = 0; i < length; i++) {
				result.add(childNodes.item(i));
			}
		}
		return result;
	}
}

class Attribute {

	private String name;
	private String value;

	Attribute(String name, String value) {
		this.name = name;
		this.value = value;
	}

	String getName() {
		return this.name;
	}

	String getValue() {
		return this.value;
	}

	@Override
	public String toString() {
		return "name=" + this.name + "\tvalue=" + this.value;
	}
}


