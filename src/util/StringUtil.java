package util;

public class StringUtil {

	public static String cleanString(String input) {
		if (input == null) {
			return null;
		}
		input = input.trim();
		return input.isEmpty() ? null : input;
	}
}
