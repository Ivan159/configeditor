package util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.stream.Stream;

public class FileUtil {

	public static Stream<String> getLinesStream(Path path) throws IOException {
		if (Files.isReadable(path)) {
			return Files.lines(path);
		}
		throw new IOException("Path " + path + " is not readable");
	}

	public static void writeFile(Path path, Iterable<String> input) throws IOException {
		if (Files.notExists(path)) {
			Files.createFile(path);
		}
		if (Files.isWritable(path)) {
			Files.write(path, input);
		}
	}

	public static void writeFile(Path path, String input) throws IOException {
		writeFile(path, Arrays.asList(input.split(System.getProperty("line.separator"))));
	}
}
