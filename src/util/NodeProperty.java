package util;

import org.w3c.dom.Node;

public class NodeProperty {

	public static NodeProperty createSystemProperty(Node node) {
		if (node.getNodeName()
				.equals("system-property")) {
			return new NodeProperty(node, PropertyType.SYSTEM_PROPERTY);
		}
		return null;
	}

	private enum PropertyType {
		DB_RESSOURCE,
		CP_PROPERTY,
		SYSTEM_PROPERTY,
		LDAP_SETTING_PROPERTY,
		LDAP_GROUP_PROPERTY
	}

	private Node node;
	private PropertyType propertyType;

	private NodeProperty(Node node, PropertyType propertyType) {
		this.node = node;
		this.propertyType = propertyType;
	}

	public static NodeProperty createConnectionPoolProperty(Node node) {
		if (node.getNodeName()
				.equals("property")) {
			return new NodeProperty(node, PropertyType.CP_PROPERTY);
		} else if (node.getNodeName()
				.equals("jdbc-resource")) {
			return new NodeProperty(node, PropertyType.DB_RESSOURCE);
		}
		return null;
	}

	public static NodeProperty createLdapSettingProperty(Node node) {
		if (node.getNodeName()
				.equals("property")) {
			return new NodeProperty(node, PropertyType.LDAP_SETTING_PROPERTY);
		}
		return null;
	}

	public static NodeProperty createLdapGroupProperty(Node node) {
		if (node.getNodeName()
				.equals("property")) {
			return new NodeProperty(node, PropertyType.LDAP_GROUP_PROPERTY);
		}
		return null;
	}

	public String getName() {
		switch (this.propertyType) {
			case DB_RESSOURCE:
				return this.node.getAttributes()
						.getNamedItem("jndi-name")
						.getNodeValue();
			case SYSTEM_PROPERTY:
			case CP_PROPERTY:
			case LDAP_GROUP_PROPERTY:
			case LDAP_SETTING_PROPERTY:
				return this.node.getAttributes()
						.getNamedItem("name")
						.getNodeValue();
			default:
				return null;
		}
	}

	public String getValue() {
		switch (this.propertyType) {
			case DB_RESSOURCE:
				return this.node.getAttributes()
						.getNamedItem("pool-name")
						.getNodeValue();
			case SYSTEM_PROPERTY:
			case CP_PROPERTY:
			case LDAP_SETTING_PROPERTY:
			case LDAP_GROUP_PROPERTY:
				return this.node.getAttributes()
						.getNamedItem("value")
						.getNodeValue();
			default:
				return null;
		}

	}

	public Node getNode() {
		return this.node;
	}

	public void setName(String name) {
		switch (this.propertyType) {
			case SYSTEM_PROPERTY:
			case CP_PROPERTY:
			case LDAP_SETTING_PROPERTY:
			case LDAP_GROUP_PROPERTY:
				this.node.getAttributes()
						.getNamedItem("name")
						.setNodeValue(name);
				break;
			case DB_RESSOURCE:
				this.node.getAttributes()
						.getNamedItem("jndi-name")
						.setNodeValue(name);
				break;
		}

	}

	public void setValue(String value) {
		switch (this.propertyType) {
			case SYSTEM_PROPERTY:
			case CP_PROPERTY:
			case LDAP_SETTING_PROPERTY:
			case LDAP_GROUP_PROPERTY:
				this.node.getAttributes()
						.getNamedItem("value")
						.setNodeValue(value);
				break;
			case DB_RESSOURCE:
				this.node.getAttributes()
						.getNamedItem("pool-name")
						.setNodeValue(value);
				break;
		}
	}

	public void setNode(Node node) {
		this.node = node;
	}

	@Override
	public String toString() {
		return this.node.getNodeName() + "\t" + this.getName() + "\t" + this.getValue();
	}
}
