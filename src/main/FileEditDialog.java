package main;

import java.io.IOException;
import java.io.StringWriter;
import java.net.URL;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import controller.FileEditWindowController;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import util.NodeProperty;
import util.XMLUtil;

public class FileEditDialog extends Dialog<String> {

	private FileEditWindowController controller;
	private XMLUtil xmlUtil;
	public Map<String, NodeProperty> dbResourceNodeMap;
	public Map<String, List<NodeProperty>> dbConnectionPoolPropertyMap;
	public Map<String, NodeProperty> systemPropertiesMap;
	public NodeProperty dialectProperty;
	public List<String> availableDialects;
	public Map<String, NodeProperty> ldapSettingsMap;
	public Map<String, NodeProperty> ldapGroupsMap;

	public FileEditDialog(Path filePath) {
		try {
			this.setResizable(true);
			this.setHeight(800D);
			this.getDialogPane()
					.setPrefHeight(800D);
			this.setTitle("ConfigEditor 1.4");
			this.xmlUtil = new XMLUtil(filePath);
			this.dbResourceNodeMap = this.xmlUtil.getDBResourceNodes()
					.stream()
					.map(NodeProperty::createConnectionPoolProperty)
					.filter(Objects::nonNull)
					.collect(Collectors.toMap(NodeProperty::getName, Function.identity()));
			this.dbConnectionPoolPropertyMap = new HashMap<>();
			this.xmlUtil.getConnectionPools()
					.forEach(node -> {
						String name = node.getAttributes()
								.getNamedItem("name")
								.getNodeValue();
						List<NodeProperty> properties = new ArrayList<>();
						for (int i = 0; i < node.getChildNodes()
								.getLength(); i++) {
							NodeProperty nodeProperty = NodeProperty.createConnectionPoolProperty(node.getChildNodes()
									.item(i));
							if (nodeProperty != null) {
								properties.add(nodeProperty);
							}
						}
						this.dbConnectionPoolPropertyMap.put(name, properties);
					});
			this.systemPropertiesMap = new HashMap<>();
			this.availableDialects = new ArrayList<>();
			this.xmlUtil.getSystemProperties()
					.forEach(node -> {
						NodeProperty property = NodeProperty.createSystemProperty(node);
						if (property.getName()
								.equals("hibernate.dialect")) {
							this.dialectProperty = property;
						} else if (property.getName()
								.startsWith("dummy.hibernate.dialect.")) {
							this.availableDialects.add(property.getValue());
						} else {
							this.systemPropertiesMap.put(property.getName(), property);
						}
					});
			List<Node> ldapSettings = this.xmlUtil.getLdapSettings();
			this.ldapSettingsMap = ldapSettings.stream()
					.map(NodeProperty::createLdapSettingProperty)
					.filter(Objects::nonNull)
					.collect(Collectors.toMap(NodeProperty::getName, Function.identity()));

			List<Node> ldapGroups = this.xmlUtil.getLdapGroups();
			this.ldapGroupsMap = ldapGroups.stream()
					.map(NodeProperty::createLdapGroupProperty)
					.filter(Objects::nonNull)
					.collect(Collectors.toMap(NodeProperty::getName, Function.identity()));

			FXMLLoader loader = new FXMLLoader();
			loader.setController(new FileEditWindowController(this));
			this.controller = loader.getController();
			String resource = "/view/FileEditWindow.fxml";
			URL res = this.getClass()
					.getResource(resource);
			loader.setLocation(res);
			this.getDialogPane()
					.setContent(loader.load());
		} catch (IOException | ParserConfigurationException | SAXException e) {
			e.printStackTrace();
		}
		this.getDialogPane()
				.getButtonTypes()
				.add(new ButtonType("OK", ButtonData.OK_DONE));
		this.getDialogPane()
				.getButtonTypes()
				.add(new ButtonType("Cancel", ButtonData.CANCEL_CLOSE));
		this.setResultConverter(param -> {
			if (param.getButtonData() == ButtonData.OK_DONE) {
				if (this.controller.acceptChanges()) {
					try {
						DOMSource domSource = new DOMSource(this.xmlUtil.getDocument());
						Transformer transformer = TransformerFactory.newInstance()
								.newTransformer();
						StringWriter sw = new StringWriter();
						StreamResult sr = new StreamResult(sw);
						transformer.transform(domSource, sr);
						return sw.toString();
					} catch (TransformerException e) {
						e.printStackTrace();
					}
				}
				System.out.println("no changes applied");
			}
			return null;
		});
	}
}
