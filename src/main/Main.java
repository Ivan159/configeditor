package main;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

import controller.FileSelectWindowController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import util.FileUtil;

public class Main extends Application {

	@Override public void start(Stage primaryStage) throws IOException {

		FXMLLoader fxmlLoader = new FXMLLoader();
		List<String> args = this.getParameters().getRaw();
		Path path = null;
		if (!args.isEmpty()) {
			path = Paths.get(args.get(0));
		}
		if (path != null && Files.isRegularFile(path)) {
			Optional<String> result;
			result = new FileEditDialog(path).showAndWait();
			Path finalPath = path;
			result.ifPresent(r -> {
				try {
					FileUtil.writeFile(finalPath, r);
					Platform.exit();
				} catch (IOException e) {
					e.printStackTrace();
				}
			});
		} else {
			String resource = "/view/FileSelectWindow.fxml";
			URL res = this.getClass().getResource(resource);
			fxmlLoader.setLocation(res);
			Parent root = fxmlLoader.load();
			FileSelectWindowController controller = fxmlLoader.getController();
			controller.setStage(primaryStage);

			Scene scene = new Scene(root);
			primaryStage.setScene(scene);
			primaryStage.setTitle("Choose editable XML...");
			primaryStage.show();
		}
	}
}
