rem build module jre:
jlink --module-path "%JAVA_HOME_MODS%";"%PATH_TO_FX_MODS%";"..\mods\production" --add-modules ConfigEditorGlassfish --compress=2 --output ../mods/Tools/ConfigEditorGlassfish/jre
rem create nested runEditor.bat:
cd..\mods
echo start jre\bin\javaw -m ConfigEditorGlassfish/main.Main ..\..\%%1 > Tools\ConfigEditorGlassfish\runEditorGlassfish.bat
rem create main runEditor.bat:
echo rem PATH_TO_DOMAIN_XML: only relative paths accepted> runEditorGlassfish.bat
echo @set PATH_TO_DOMAIN_XML=Server\Glassfish\glassfish\domains\domain1\config\domain.xml >> runEditorGlassfish.bat
echo cd Tools\ConfigEditorGlassfish >> runEditorGlassfish.bat
echo runEditorGlassfish.bat %%PATH_TO_DOMAIN_XML%% >> runEditorGlassfish.bat
echo cd ..\..
rem zip files:
tar -a -c -f ConfigEditorGlassfish_v1.4.zip Tools runEditorGlassfish.bat
rem cleanup again:
rmdir /Q /S Tools
del runEditorGlassfish.bat